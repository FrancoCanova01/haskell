module Lib
    ( someFunc
    ) where

someFunc :: IO ()
someFunc = putStrLn "someeeeeFunc"

{- ///////////////////////  CHAPTER 2 /////////////////////// -}

{- List comprehension 
[x*2 | x <- [1..10]]
[x*2 | x <- [1..10], x*2 >= 12]
[ x | x <- [50..100], x `mod` 7 == 3]
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
[ x | x <- [10..20], x /= 13, x /= 15, x /= 19]
[ x*y | x <- [2,5,10], y <- [8,10,11]]
[ x*y | x <- [2,5,10], y <- [8,10,11], x*y > 50]
length' xs = sum [1 | _ <- xs]
removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]
[ [ x | x <- xs, even x ] | xs <- xxs]
-}

{- Tuples
zip [1 .. 5] ["one", "two", "three", "four", "five"]
zip [1..] ["apple", "orange", "cherry", "mango"]
-}

{-
Which right triangle that has integers for all sides and all sides 
equal to or smaller than 10 has a perimeter of 24?

let triangles = [ (a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10] ]

No we need to filter all triples that don't fit criteria

1) Right triangles
let rightTriangles = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2]
2) Right triangles with perimeter 24
let rightTriangles' = [ (a,b,c) | c <- [1..10] , b <- [1..c] , a <- [1..b] , a^2 + b^2 == c^2, a+b+c == 24]
-}

{- ///////////////////////  CHAPTER 3 /////////////////////// -}

{-
Type /= Type Variable /= Type classes


Type classes:
- If a type is a part of a typeclass, that means that it supports and implements the
behavior the typeclass describes.

- Everything before the => symbol is called a
class constraint.

- Members of Show can be presented as strings. The most used function that deals with the 
Show typeclass is show. It takes a value whose type is a member of Show and presents it to us as a string.

show 5.334
"5.334"

- Read is sort of the opposite typeclass of Show. The read function takes a string and returns a type
which is a member of Read.

read "True" || False
True

read "[1,2,3,4]" ++ [3]
[1,2,3,4,3]

read "4"
ERROR -> Haskell cant infer by itself what type class to "cast" the "4" to. For that we need 
to use explicit type annotations like below.

read "4" :: Int
4

read "(3, 'a')" :: (Int, Char)
(3, 'a')

- Classes belonging to Bounded class type have an upper and a lower bound.

minBound :: Int
-9223372036854775808

maxBound :: Char
'\1114111'

maxBound :: Bool
True

minBound :: Bool
False

- Num is a numeric typeclass. Its members have the property of being able to act like numbers.

20 :: Int
20

20 :: Integer
20

20 :: Float
20.0

20 :: Double
20.0
-}



{- ///////////////////////  CHAPTER 4 /////////////////////// -}



