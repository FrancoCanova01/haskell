# helloworld (Intro to stack)

Follow along: https://docs.haskellstack.org/en/stable/GUIDE/    

## Notes
### Stack.yaml
- Packages tells stack which local packages to build.
- Resolver tells stack how to build your package (which GHC version to use, versions of package dependencies, and so on). Its the version of LTS resolver to use to get snapshots.
### Package.yaml
- The default behaviour is to generate the .cabal file from this package.yaml, and accordingly you should not modify the .cabal file.
### Packages
- Stack has some default packages available for install (also known as snapshots). If you want a package that is not in this initial set of packages use "Curated Packages" by using <code>extra-deps</code> field in <code>stack.yaml</code>
- Using open source package: <code>stack unpack --package--</code>. Use stack init to setup a <code>stack.yaml</code> file for the unpacked project
- Sometimes multiple packages in your project may have conflicting requirements (and stack init will fail to create the appropriate <code>stack.yaml</code>). What can help you resolve these issues is using <code>--omit-packages</code> flag.
- Stack has the concept of multiple databases. A database consists of a GHC package database (which contains the compiled version of a library), executables, and a few other things as well. Databases in stack are layered. For example, the database listing we just gave is called a local database. That is layered on top of a snapshot database, which contains the libraries and executables specified in the snapshot itself. Finally, GHC itself ships with a number of libraries and executables, which forms the global database. Run <code>stack exec -- ghc-pkg list</code> to see what packages are in what database. Anything which is not installed from a snapshot ends up in the local database. This includes: your own code, extra-deps, and in some cases even snapshot packages, if you modify them in some way. There are multiple snapshot databases available (LTS o Nightly, the ones you can change in resolver inside <code>stack.yaml</code>)
- In addition to local directories, you can also refer to packages available in a Git repository or in a tarball over HTTP/HTTPS. This can be useful for using a modified version of a dependency that hasn't yet been released upstream.
### Other Notes
- Where stack stores various files: <code>stack path</code> (can be convenient for scripting.)
- For project templates: https://github.com/commercialhaskell/stack-templates/wiki

