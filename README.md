# Haskell

A repo where I play with Haskell programming language and learn functional programming concepts.

Long term objective is to move into Plutus (Haskell based) and code on Cardano Blockchain.
